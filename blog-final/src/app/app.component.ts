import {Component} from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() {
    const config = {
      apiKey: 'AIzaSyCZ-dAqK505vQ69B_yMm9Fj5EwqWJe4h6A',
      authDomain: 'blog-final-d6c47.firebaseapp.com',
      databaseURL: 'https://blog-final-d6c47.firebaseio.com',
      projectId: 'blog-final-d6c47',
      storageBucket: '',
      messagingSenderId: '522697469504'
    };
    firebase.initializeApp(config);
  }
}
