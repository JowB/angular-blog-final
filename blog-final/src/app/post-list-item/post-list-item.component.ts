import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {PostsService} from '../services/posts.service';
import {Post} from '../models/post';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit, OnDestroy {

  posts: Post[];
  post: Post = new Post();
  postsSubscription: Subscription;

  constructor(private postsService: PostsService) {}

  ngOnInit() {
    this.postsSubscription = this.postsService.postsSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );
    this.postsService.getPosts();
    this.postsService.emitPosts();
  }

  iLoveIt (index: number) {
    this.postsService.love(index);
  }

  iDontLoveIt (index: number) {
    this.postsService.noLove(index);
  }

  onDeletePost(index: number) {
    this.postsService.removePost(index);
  }

  ngOnDestroy() {
    this.postsSubscription.unsubscribe();
  }
}
