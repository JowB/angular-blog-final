import { Injectable } from '@angular/core';
import {Post} from '../models/post';
import {Subject} from 'rxjs/Subject';
import * as firebase from 'firebase';
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable()
export class PostsService {

  posts: Post[] = [];
  postsSubject = new Subject<Post[]>();

  constructor() { }

  emitPosts() {
    this.postsSubject.next(this.posts);
  }

  savePosts() {
    firebase.database().ref('/posts').set(this.posts);
  }

  getPosts() {
    firebase.database().ref('/posts')
      .on('value', (data: DataSnapshot) => {
        this.posts = data.val() ? data.val() : [];
        this.emitPosts();
      });
  }

  createNewPost(newPost) {
    this.posts.push(newPost);
    this.savePosts();
    this.emitPosts();
  }

  removePost(index) {
    this.posts.splice(index, 1);
    this.savePosts();
    this.emitPosts();
  }

  love(index) {
    this.posts[index].loveIts++;
    this.savePosts();
    this.emitPosts();
  }

  noLove(index) {
    this.posts[index].loveIts--;
    this.savePosts();
    this.emitPosts();
  }
}
